#by Nathan Cantor
#Newtons Method Program
#Just as every cop is a criminal and all the sinners saints
#1/16/2019

#This program produces a fractal based on a function given by the user and newtons method

#To run this you must install sympy
#in command prompt or terminal you can use the command:
#pip install sympy
import cmath
from graphics import *
import sys
import math
import sympy as sp
import random

z = sp.symbols('z')

#This function takes a function as input using sympy to be used later. Also makes sure the
#it is a valid function and then returns it
def takeF():
    f = input('enter a function of z here: ')
    while True:
        func = sp.lambdify(z,f)
        try:
            func(3)
            break
        except NameError:
            f = input('you imputed the function incorrectly, try again: ')
            func = sp.lambdify(z,f)
        except ValueError:
            f = input('you imputed the function incorrectly, try again: ')
            func = sp.lambdify(z,f)
        except ZeroDivisionError:
            f = input('you imputed the function incorrectly, try again: ')
            func = sp.lambdify(z,f)
    return f
#newtons methods
#also cathces an overflow error, only really comes into play with exponentiation by complex powers
def newtons(z0,func,der):
    while (True):
        try:
            der(z0)
            break
        except OverflowError:
            return z0
    a = der(z0)
    if a == 0:
        a = 0.00000000000000000000001
    return z0 - (func(z0)/a)
#does newtons method a bunch of times
def iterateNewtons(z0, numIterates,func,der):
    z = z0
    for i in range(numIterates):
        z = newtons(z,func,der)
    return z
#takes the max number producable with a 6 digit hexadecimal number
#divides it by n and produces p
#produces a list containing every pth hex number
#also formats the numbers
def genColor(n):
    big = 16777215
    inc = int(big/n)
    acc = 0
    lst = []
    for i in range(n+1):
        lst.append(acc)
        acc += inc
    for i in range(len(lst)):
         lst[i] = str(hex(lst[i]))
    lst.remove('0x0')
    for i in range(len(lst)):
        lst[i] = '#' + lst[i][2:]
    for i in range(len(lst)):
        while len(lst[i]) < 7:
            lst[i] = '#0' + lst[i][1:]
    return(lst)
#finds the root that every z is closest to in the list roots
def getClosestRoot(z,roots):
    cd = sys.maxsize
    r = 0
    for i in range(len(roots)):
        if cd > abs(((z.real-roots[i].real)**2+(z.imag-roots[i].imag)**2)**(1/2)):
            r = i
            cd = abs(((z.real-roots[i].real)**2+(z.imag-roots[i].imag)**2)**(1/2))
    return(r)
#assigns every point a color and draws them all
def fillGraph(func,der,w,xmin,xmax,ymin,ymax,roots,iterYeets):
    xstep=((xmax-xmin)/w.getWidth())
    ystep=((ymax-ymin)/w.getHeight())
    x=xmin
    y=ymin
    n = 0
    clr = genColor(len(roots))
    while x<=xmax:
        y=ymin
        while y<=ymax:
            c = clr[getClosestRoot(iterateNewtons(complex(x,y),iterYeets,func,der),roots)]
            w.plot(x,y,c)
            y+=ystep
            n+=1
            # if n%50000 == 0:
            #     w.update()
        x+=xstep
    w.update()


#sets the coords for the window
def axisShift(xmin,xmax,ymin,ymax,win):
    win.setCoords(xmin,ymin,xmax,ymax)


#does some jank garb and returns a list containing a list of all the root
#basically chooses a bunch of random points and applies newtons method to them
def findRoot(func,der,win,xmin,xmax,ymin,ymax):
    x = random.uniform(xmin,xmax)
    y = random.uniform(ymin,ymax)
    rts = []
    for i in range(500):
        x = random.uniform(xmin,xmax)
        y = random.uniform(ymin,ymax)
        cr = iterateNewtons(complex(x,y),1000,func,der)
        cr = complex(round(cr.real,4),round(cr.imag,4))
        known = False
        for k in rts:
            if cr == k:
                known = True
        if known == False:
            rts.append(cr)
    return rts

#2-click zoom, returns list containing new [xmin,xmax,ymin,ymax]
#also redraws the function for the new bounds
def zoomIn(func,der,win,xmin,xmax,ymin,ymax,roots,iterYeets):
    print('yate')
    print('click to chose the first corner of your new window')
    p1 = win.getMouse()
    print('click to chose the second corner of your new window')
    p2 = win.getMouse()
    x1=p1.getX()
    y1=p1.getY()
    x2=p2.getX()
    y2=p2.getY()
    if x1 > x2:
        nxmax = x1
        nxmin = x2
    else:
        nxmin = x1
        nxmax = x2
    if y1 > y2:
        nymax = y1
        nymin = y2
    else:
        nymin = y1
        nymax = y2
    win.clear()
    print('click the graphwin after it finishes drawing to advance to program')
    axisShift(nxmin,nxmax,nymin,nymax,win)
    fillGraph(func,der,win,nxmin,nxmax,nymin,nymax,roots,iterYeets)
    win.getMouse()
    return([nxmin,nxmax,nymin,nymax])

def main():
    print('my personal reccomendations for entering functions are those where z \nis raised to some complex exzponent \nhaving multiple zs raised to complex powers \nalso produces interesting results')
    inps = '4'
    xmin = -10
    xmax = 10
    ymin = -10
    ymax= 10
    iterYeets = 50
    win = GraphWin('window',height = 600, width = 600)
    win.setCoords(-10,10,-10,10)
    while(inps == '4'):
        win.clear()
        win.setBackground('black')
        f = takeF()
        func = sp.lambdify(z,f)
        der = sp.lambdify(z,sp.diff(f))
        axisShift(xmin,xmax,ymin,ymax,win)
        lst = findRoot(func,der,win,xmin,xmax,ymin,ymax)
        print(lst)
        fillGraph(func,der,win,xmin,xmax,ymin,ymax,lst,iterYeets)
        win.update()
        inps = (str(input('list of commands \n1: 2-click zoom \n2: change number of Iterates \n3: zoom out to original domain and range \n4: draw a new function \n  ')))
        while (inps == '1' or inps == '2' or inps == '3'):
            if inps == '1':
                coord = zoomIn(func,der,win,xmin,xmax,ymin,ymax,lst,iterYeets)
                xmin = coord[0]
                xmax = coord[1]
                ymin = coord [2]
                ymax = coord[3]
                inps = (str(input('list of commands \n1: 2-click zoom \n2: change number of Iterates \n3: zoom out to original domain and range \n4: draw a new function \n  ')))
            if inps == '2':
                iterYeets = int(input('please enter the number of iterates of newtons you would like \n (higher number makes a more accurate picture \nlower number to make it look more like the seventies): '))
                win.clear()
                fillGraph(func,der,win,xmin,xmax,ymin,ymax,lst,iterYeets)
                inps = (str(input('list of commands \n1: 2-click zoom \n2: change number of Iterates \n3: zoom out to original domain and range \n4: draw a new function \n  ')))
            if inps == '3':
                xmin = -10
                xmax = 10
                ymin = -10
                ymax = 10
                axisShift(xmin,xmax,ymin,ymax,win)
                win.clear()
                fillGraph(func,der,win,xmin,xmax,ymin,ymax,lst,iterYeets)
                inps = (str(input('list of commands \n1: 2-click zoom \n2: change number of Iterates \n3: zoom out to original domain and range \n4: draw a new function \n  ')))



if __name__ == '__main__':
    main()
