from math import *
from graphics import *
import wolframalpha

client = wolframalpha.Client('3KPRYT-6Q3YAQ7GKV')

def genColor(n):
    big = 16777215
    inc = int(big/n)
    acc = 0
    lst = []
    for i in range(n+1):
        lst.append(acc)
        acc += inc
    for i in range(len(lst)):
         lst[i] = str(hex(lst[i]))
    lst.remove('0x0')
    for i in range(len(lst)):
        lst[i] = '#' + lst[i][2:]
    for i in range(len(lst)):
        while len(lst[i]) < 7:
            lst[i] = '#0' + lst[i][1:]
    return(lst)


def drawLine(win, startPoint, L, thetaDegrees,color = 'black', width = 1):
    dx = L*cos(radians(thetaDegrees))
    dy = L*sin(radians(thetaDegrees))
    endPoint = Point(startPoint.getX()+dx,startPoint.getY()+dy)
    line = Line(startPoint, endPoint)
    line.setFill(color)
    line.setWidth(width)
    line.draw(win)
    startPoint.move(dx,dy)


def queryInches(dInches):
    global client
    return client.query(str(dInches) + "inches")

def giveInterestingLengths(dInches):
    result = queryInches(dInches)
    for pod in result.pods:
        if (pod.title == "Comparisons as length"):
            print("Roughly Equal Lengths")
            for sub in pod.texts:
                print(sub)
        if (pod.title == "Comparisons as height"):
            print("Roughly Equal Heights")
            for sub in pod.texts:
                print(sub)
        if (pod.title == "Comparisons as Distance"):
             for sub in pod.texts:
                print(sub)

            
	

