# Nonlinear Dynamics
# December 2018
# By Nathan Cantor
#
# -------------------
#
# Julia Set

from graphics import *
from cmath import *
import numpy as np
from random import random 

#brute force mandelbrot set generating algoritm
#both this and the julia set algorithms basically just go through every point
#and iterate, checking each's conditions
def mbrot(win,xmin,xmax,ymin,ymax,maxIters,maxRadius):
    iterCount = 0
    r = xmin
    xstep = 1
    ystep = 1
    step = 0
    p = 0
    stepSize = (((xmax-xmin)*(ymax-ymin))**(1/2)/(((win.getWidth())*(win.getHeight())))**(1/2))
    numsteps=0
    while r <xmax:
        i = ymin
        r+=stepSize
        while i <ymax:
            numsteps+=1
            i+=stepSize
    #steps in entire canvas
    dims = numsteps
    pts = np.zeros((dims,2),dtype = object)
    p = 0
    r = xmin
    while r < xmax:
        i = ymin
        xstep += 1
        ystep = 1
        while i < ymax:
            thisz = complex(r,i)
            pts[step,0] = thisz
            #z = complex(random(),random())
            z = complex(0,0)
            c = complex(r,i)
            iterCount = 0
            while iterCount < maxIters and abs(z) < maxRadius:
                z = f(z,c)
                iterCount += 1
            if iterCount == maxIters:
                pts[step,1]='red'
            i += stepSize
            if iterCount < maxIters:
                pts[step,1] = getColor(iterCount,maxIters)
            step += 1
        r += stepSize
        #win.update()
    return(pts)

#brute force julia set algorithm
def juliaBrute(win,xmin,xmax,ymin,ymax,maxIters,maxRadius,c):
    iterCount = 0
    stepSize = (((xmax-xmin)*(ymax-ymin))**(1/2)/(((win.getWidth())*(win.getHeight())))**(1/2))
    numsteps=0
    r = xmin
    while r <xmax:
        i = ymin
        r+=stepSize
        while i <ymax:
            numsteps+=1
            i+=stepSize
    #steps in entire canvas
    dims = numsteps
    step = 0
    pts = np.zeros((dims,2),dtype = object)
    r = xmin
    while r < xmax:
        i = ymin
        while i < ymax:
            z = complex(r,i)
            pts[step,0] = z
            iterCount = 0
            while iterCount < maxIters and abs(z) < maxRadius:
                z = f(z,c)
                iterCount += 1
            if iterCount == maxIters:
                # complex(r,i) is in the filled julia set
                pts[step,1]='black'
            i += stepSize
            if iterCount < maxIters:
                pts[step,1]=getColor(iterCount,maxIters)
            step+=1
        r += stepSize
    return(pts)        

#plots points from a list 
def plotPts(win,pts):
    n = 0
    for i in range(np.size(pts,0)-1):
        z = pts[i,0]
        win.plot(z.real,z.imag,pts[i,1])
        n+=1
        if n % 10000 == 0:
            win.update()
    win.update()

#plots the points in a random order
#takes a long time for no reason
def plotPtsJank(win,pts):
    n = 0
    np.random.shuffle(pts)
    for i in range(np.size(pts,0)-1):
        z = pts[i,0]
        win.plot(z.real,z.imag,pts[i,1])
        n+=1
        if n % 100000 == 0:
            win.update()
    win.update()



#basically you're going to iterate some randome point until it is definately one the set
#then you're going to iterate that point a bunch and make a list out of it 
def juliaInverse(win,xmin,xmax,ymin,ymax,c):
    pts = np.zeros((5000,2),dtype = object)
    z = complex((xmin+xmax*random()),(ymin+ymax*random()))
    for i in range (10000):
        z = invf(z,c)
    for i in range(5000):
        z = invf(z,c)
        pts[i] = [z,'black']

    return pts
#the function used for iteration
def f(z,c):
    return z*z + c

def invf(z,c):
    i = 1
    if random() < .5:
        i = -1
    return i * (z-c) ** 0.5


#gets color based on how many iterates have passed
def getColor (n,maxIters):
    colorVal = int((255 * (1+n/4)))
    c = color_rgb(colorVal,colorVal,colorVal)
    return c

#passes list of coordinates and changes a window's coords
def zoomIn(win):
    print('click to chose the first corner of your new window')
    p1 = win.getMouse()
    print('click to chose the second corner of your new window')
    p2 = win.getMouse()
    x1=p1.getX()
    y1=p1.getY()
    x2=p2.getX()
    y2=p2.getY()
    if x1 > x2:
        nxmax = x1
        nxmin = x2
    else:
        nxmin = x1
        nxmax = x2
    if y1 > y2:
        nymax = y1
        nymin = y2
    else:
        nymin = y1
        nymax = y2
    win.clear()
    win.setCoords(nxmin,nymin,nxmax,nymax)
    return([nxmin,nxmax,nymin,nymax])



def axisShift(win,xmin,xmax,ymin,ymax):
    win.setCoords(xmin,ymin,xmax,ymax)

#main
def main():
    inp = '0'
    c = complex(1,0)
    #win = GraphWin(title = 'A Certain Boxy Graph Window', height = 600, width = 600, autoflush = False)
    mbw = GraphWin(title = 'Mandelbrot set', height = 600, width = 600, autoflush = False)
    jsw = GraphWin(title = 'Julia set', height = 600, width = 600, autoflush = False)
    xmin = -2
    xmax = 2
    ymin = -2
    ymax = 2
    jsw.setCoords(-2,-2,2,2)
    mbw.setCoords(-2,-2,2,2)
    #win.setCoords(-5,-5,5,5)
    maxIters = 100
    maxRadius = 2.0
    print("""           welcome to my mandlbrot program
            _________________________________
            The mandelbrot set is drawing as you read this
            After the mandelbrot set in finished drawing,
            click a point in the window to draw the julia set
            associated with that imaginary number (if the 
            point clicked is 'c' then the julia set for z**2 + c will
            be generated) after drawing a rough version of that julia set.
            After that further instructions will appear in
            the terminal

            Base max iterates = 100""")
    mb = mbrot(mbw,xmin,xmax,ymin,ymax,maxIters,maxRadius)
    #np.random.shuffle(mb)
    plotPts(mbw,mb)
    a = mbw.getMouse()
    juuli = juliaInverse(jsw,xmin,xmax,ymin,ymax,complex(a.getX(),a.getY()))
    plotPts(jsw,juuli)
    juul = juliaBrute(jsw,xmin,xmax,ymin,ymax,maxIters,maxRadius,complex(a.getX(),a.getY()))
    plotPts(jsw,juul)
    print("""
            Enter the number associated with the action you desire
            ______________________________________________________
            1: Zoom into mandelbrot set
            2: Zoom into julia set
            (both zooms are 2-click zooms)
            3: Choose another point on the mandelbrot set from which to draw a julia set
            4: Increase number of iterations
            5: Redraw mandelbrot set (with default zoom)
            6: Redraw current julia set (with default zoom)
            7: Redraw mandelbrot set in a way that takes way longer (for no discernable reason)
            8: End the program""")
    while not inp == '8':
        if inp == '1' :
            new = zoomIn(mbw)
            bxmin = new[0]
            bxmax = new[1]
            bymin = new[2]
            bymax = new[3]
            nmb =  mbrot(mbw,bxmin,bxmax,bymin,bymax,maxIters,maxRadius)
            mbw.clear()
            plotPts(mbw,nmb)
        elif inp == '2':
            new = zoomIn(jsw)
            bxmin = new[0]
            bxmax = new[1]
            bymin = new[2]
            bymax = new[3]
            njuul =  juliaBrute(jsw,bxmin,bxmax,bymin,bymax,maxIters,maxRadius)
            jsw.clear()
            plotPts(jsw,njuul)
        elif inp == '3':
            jsw.clear()
            axisShift(jsw,xmin,xmax,ymin,ymax)
            print('click somewhere in the mandelbrot window to chose another point')
            a = mbw.getMouse()
            print('dab')
            juuli = juliaInverse(jsw,xmin,xmax,ymin,ymax,complex(a.getX(),a.getY()))
            plotPts(jsw,juuli)
            juul = juliaBrute(jsw,xmin,xmax,ymin,ymax,maxIters,maxRadius,complex(a.getX(),a.getY()))
            plotPts(jsw,juul)
        elif inp == '4':
            while True:
                maxIters = str(input('enter the new number of max iterates: '))
                try:
                    int(maxIters)
                    break
                except ValueError:
                    maxIters = str(input('That is not a valid input try again: '))
            maxIters = int(maxIters)
        elif inp == '5':
            mbw.clear()
            axisShift(mbw,xmin,xmax,ymin,ymax)
            plotPts(mbw,mb)
        elif inp == '6':
            jsw.clear()
            axisShift(jsw,xmin,xmax,ymin,ymax)
            juuli = juliaInverse(jsw,xmin,xmax,ymin,ymax,complex(a.getX(),a.getY()))
            plotPts(jsw,juuli)
            juul = juliaBrute(jsw,xmin,xmax,ymin,ymax,maxIters,maxRadius,complex(a.getX(),a.getY()))
            plotPts(jsw,juul)
        elif inp == '7':
            mbw.clear()
            axisShift(mbw,xmin,xmax,ymin,ymax)
            plotPtsJank(mbw,mb)
        inp = str(input('enter another input: '))


if __name__ == "__main__":
    main()
