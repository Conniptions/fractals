
#Nathan Cantor
#Nonlinear Dynamics
#-------------------
import math
from graphics import *

#Program that draws a bifurcation diagram, a cobweb diagram, and a time series graphself.
#The time series graph and combweb diagram are based on user inputs
#The Bifurcation diagram can be zoom in and out on

#This is the function representing the logistic map
def logisticMap(x,R):
    return R*x*(1-x)

#This function iterates through the logistic map plugging in the ouput value
#This also makes a list (toPrint) with an inputed number of values
def getIteratesPrint(R,x,numTrans,numIter):
    xc = x
    toPrint = []
    for i in range(numTrans):
        xc = logisticMap(xc,R)
    for i in range(numIter):
        xc = logisticMap(xc,R)
        toPrint.append(round(xc,5))
    return toPrint

#same but unformated
def getIterates(R,x,numTrans,numIter):
    xc = x
    iters = []
    for i in range(numTrans):
        xc = logisticMap(xc,R)
    for i in range(numIter):
        xc = logisticMap(xc,R)
        iters.append(xc)
    return iters
#returns the period of the logistic map with the given values (supposing that there is a high enough transcient)
def getPeriod(iters):
    for j in range(len(iters)):
        for i in  range(j+1,len(iters)):
            if iters[i] == iters[j]:
                return i-j
    return 0

#Gets a list of the fixed point or the parts of the orbit
def getPeriodLst(iters):
    periodLst = []
    for i in range(getPeriod(iters)):
        periodLst.append(iters[i])
    return periodLst

#draws bifurcation diagram
def bifurcate(win,xmin,xmax,ymin,ymax):
    if xmax > 4:
        xmax =4
    if xmin < 0:
        xmin = 0
    axisShift(xmin,xmax,ymin,ymax,win)
    R = xmin
    x = 0.001
    n = 0
    stepSize = ((xmax-xmin)/win.getWidth())/2
    while R <= xmax:
        points = getIterates(R,x,100,100)
        for i in points:
            win.plot(R,i,'black')
        R+=stepSize
        n+= 1
        if n%10 == 0:
            win.update()
    win.update()
#sets the coords for the window and draws the axis
def axisShift(xmin,xmax,ymin,ymax,win):
    win.setCoords(xmin,ymin,xmax,ymax)
#goes through a bunch of x values and plots the function at those x's
def plots(f,a,b,c,xmin,xmax,win):
    x = xmin
    stepSize = ((xmax-xmin)/win.getWidth())/10
    while x<=xmax:
        win.plot(x,f(a,b,c,x),'red')
        # p=Point(x,f(a,b,c,x))
        # p.setOutline('red')
        # p.draw(win)
        x += stepSize
    win.update()
#finds the x's of the roots of the given function

#take input of a float between given values
def getFloat(mn, mx):
    prompt = "input a float between " + mn + " and " + mx + ":  "
    if mn == 'n' and mx == 'n':
        prompt = "input a float "
    elif mn == 'n':
        prompt = "input  a float less than " + mx + " "
    elif mx == 'n':
        prompt = "input  a float greater than " + mn + " "
    #this part makes sure the inputed float is within the constraints
    inp = float(input(prompt))
    if mn == 'n' and mx == 'n':
        return inp
    elif mn == 'n':
        while inp >= float(mx):
            inp = float(input(prompt))
    elif mx == 'n':
        while inp <= float(mn):
            inp = float(input(prompt))
    else:
        while inp <= float(mn) or inp >= float(mx):
            inp = float(input(prompt))
    return inp

def getInt(mn, mx):
    prompt = "input a int between " + mn + " and " + mx + ":  "
    if mn == 'n' and mx == 'n':
        prompt = "input an int "
    elif mn == 'n':
        prompt = "input  an int less than " + mx + " "
    elif mx == 'n':
        prompt = "input  a int greater than " + mn + " "
    #this part makes sure the inputed int is within the constraints
    inp = int(input(prompt))
    if mn == 'n' and mx == 'n':
        return inp
    elif mn == 'n':
        while inp >= int(mx):
            inp = int(input(prompt))
    elif mx == 'n':
        while inp <= int(mn):
            inp = int(input(prompt))
    else:
        while inp <= int(mn) or inp >= int(mx):
            inp = int(input(prompt))
    return inp

#draws time series with lines
def timeSeriesLn(win,trann,stx,iters,R):
    xmin =0
    xmax = iters
    ymin = 0
    ymax = 1
    axisShift(xmin,xmax,ymin,ymax,win)
    itersList = getIteratesPrint(R,stx,trann,iters)
    oldPoint = Point(0,itersList[0])
    for i in range(len(itersList)):
        win.plot(i,itersList[i],'black')
        Line(oldPoint,Point(i,itersList[i])).draw(win)
        oldPoint=Point(i,itersList[i])

#Draws cobweb diagram
def cobYeet(win,stx,iters,R,xmin,xmax,ymin,ymax):
    axisShift(xmin,xmax,ymin,ymax,win)
    x = xmin
    a=0
    stepSize = ((xmax-xmin)/win.getWidth())
    while x<=xmax:
        win.plot(x,logisticMap(x,R),'red')
        win.plot(x,x,'red')
        x+= stepSize
    cur = stx
    while a <= iters:
        Line(Point(cur,cur),Point(cur,logisticMap(cur,R))).draw(win)
        Line(Point(cur,logisticMap(cur,R)),Point(logisticMap(cur,R),logisticMap(cur,R))).draw(win)
        cur = logisticMap(cur,R)
        a+=1
#zooms into a point the user clicks on, and returns the new xmin/max and ymin/max
#will zoom into a 10 times smaller window
def zoomIn(win,xmin,xmax,ymin,ymax):
    p1 = win.getMouse()
    x=p1.getX()
    y=p1.getY()
    domain = xmax-xmin
    range = ymax - ymin
    nxmin = x - domain/10
    nxmax = x + domain/10
    nymin = y - range/10
    nymax = y + range/10
    return [nxmin,nxmax,nymin,nymax]
#zooms out of a point the user clicks on, and returns the new xmin/max and ymin/max
#will zoom into a 2x larger window
def zoomOut(win,xmin,xmax,ymin,ymax):
    p1 = win.getMouse()
    x=p1.getX()
    y=p1.getY()
    domain = xmax-xmin
    range = ymax - ymin
    nxmin = x - domain*2
    nxmax = x + domain*2
    nymin = y - range*2
    nymax = y + range*2
    return [nxmin,nxmax,nymin,nymax]

#take inputs and uses them to produce cobweb and time series diagram
#also produces a bifurcation diagram independent of inputs
def main():
    com = 1
    tim = GraphWin('Time Series Graph',800,400,autoflush=True)
    cob = GraphWin('Cob Web Diagram',800,400,autoflush=True)
    bif = GraphWin('Bifurcation Diagram',800,400,autoflush=False)
    #need these so they can be changed later by zoom functions
    bxmin = 0
    bxmax = 4
    bymin = 0
    bymax = 1
    bifurcate(bif, bxmin,bxmax,bymin,bymax)
    #while loop for repition
    while com == 1:
        cob.clear()
        tim.clear()
        print('Wewcome to my pwogwam OwO *nuzzles*')
        print('Please enter an R value between 0 and 4')
        R=getFloat('0','4')
        print('Please enter a x0 value between 0 and 1')
        stx=getFloat('0','1')
        print('Please enter the number of iterations you want to see ')
        iter = getInt('0','n')
        print('Please enter the number of transcients you want')
        trans= getInt('0','n')
        cobYeet(cob, stx, iter,R, 0,1,0,1)
        timeSeriesLn(tim,trans, stx,iter,R)
        print('with and R value of ', R , 'the period is ' , getPeriod)
        print('the terms in the orbit are' , getPeriodLst(getIteratesPrint(R,stx,trans, iter)))
        com = 0
        while com != 1 and com != 5:
            print('Further commands(type the associated number to do the thing):  \n 1: enter new inputs \n 2: Zoom into Bifurcation Diagram \n 3: Zoom out of Bifurcation Diagram \n 4: reset bifurcation diagram: \n 5: end the program')
            com = int(input('enter command here: '))
            while com != 1 and com !=2 and com != 3 and com != 4 and com != 5:
                com = int(input('please enter either 1,2,3,4, or 5 '))
            #for 2 and 3
            #takes returned new scale for bifucation diagram and creates a new bifurcation diagram with the new dimesions
            if com == 2:
                print('Click somewhere on the bifurcation diagram to zoom in on that point')
                new = zoomIn(bif,bxmin,bxmax,bymin,bymax)
                bif.clear()
                bxmin = new[0]
                bxmax = new[1]
                bymin = new[2]
                bymax = new[3]
                axisShift(bxmin,bxmax,bymin,bymax,bif)
                bifurcate(bif,bxmin,bxmax,bymin,bymax)
            elif com == 3:
                print('Click somewhere on the bifurcation diagram to zoom out on that point')
                new = zoomOut(bif,bxmin,bxmax,bymin,bymax)
                bif.clear()
                bxmin = new[0]
                bxmax = new[1]
                bymin = new[2]
                bymax = new[3]
                axisShift(bxmin,bxmax,bymin,bymax,bif)
                bifurcate(bif,bxmin,bxmax,bymin,bymax)
            #resets bifurcation diagram
            elif com == 4:
                bif.clear()
                bxmin = 0
                bxmax = 4
                bymin = 0
                bymax= 1
                axisShift(bxmin,bxmax,bymin,bymax,bif)
                bifurcate(bif, bxmin,bxmax,bymin,bymax)






if __name__ == "__main__":
    main()
