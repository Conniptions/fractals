#by Nathan Cantor
#For drawing fractal trees
import time
from fractalUtils import *
from graphics import *
import math as math
import random

#Draws a fractal tree, and recurses until base case level
def drawTree(win,level,startPt,L,direction,Brangle = 20,sf = 0.7,RC=1):
            color = color_rgb(96,35,6)
            if level <= 0:
                color = 'green'
            k = []
            #This makes it so that there can be extra levels
            for i in range(int(10*(1/RC))):
                k.append(0)
            k.append(1)
            win.update()
            if level == 0:
                drawLine(win,startPt,L,direction,color,level)
            else:
                #RangleLeft = random.uniform(-90,90)
                #RangleRight = random.uniform(-90,90)
                #a bunc of randomization
                RangleLeft = random.uniform(-Brangle*RC,Brangle*RC)
                RangleRight = random.uniform(-Brangle*RC,Brangle*RC)
                LevelVar1 = 1
                LevelVar2 = 1
                if random.sample(k,1) == [1]:
                    LevelVar1 = 0
                if random.sample(k,1) == [1]:
                    LevelVar2 = 0
                LVar1 = random.uniform(L*(sf/RC),L*RC/sf)
                LVar2 = random.uniform(L*(sf/RC),L*RC/sf)
                drawLine(win,startPt,L,direction,color,level)
                startPt2 = startPt.clone()
                drawTree(win,level-LevelVar1,startPt,LVar1,direction+Brangle+RangleLeft,Brangle,sf,RC)
                drawTree(win,level-LevelVar2,startPt2,LVar2,direction-Brangle+RangleRight,Brangle,sf,RC)

def drawColorTree(win,level,startPt,L,direction,Brangle = 20,sf = 0.7,RC=1):
            color = clr[level]
            if level <= 0:
                color = 'green'
            k = []
            for i in range(int(10*(1/RC))):
                k.append(0)
            k.append(1)
            win.update()
            if level == 0:
                drawLine(win,startPt,L,direction,color,level)
            else:
                #RangleLeft = random.uniform(-90,90)
                #RangleRight = random.uniform(-90,90)
                RangleLeft = random.uniform(-Brangle*RC,Brangle*RC)
                RangleRight = random.uniform(-Brangle*RC,Brangle*RC)
                LevelVar1 = 1
                LevelVar2 = 1
                if random.sample(k,1) == [1]:
                    LevelVar1 = 0
                if random.sample(k,1) == [1]:
                    LevelVar2 = 0
                LVar1 = random.uniform(L*sf/RC,L*RC)
                LVar2 = random.uniform(L*sf/RC,L*RC)
                drawLine(win,startPt,L,direction,color,level)
                startPt2 = startPt.clone()
                drawColorTree(win,level-LevelVar1,startPt,LVar1,direction+Brangle+RangleLeft,Brangle,sf,RC)
                drawColorTree(win,level-LevelVar2,startPt2,LVar2,direction-Brangle+RangleRight,Brangle,sf,RC)

def  makeGarden(win, maxTrees,level,L,direction,Brangle = 20, sf = .5, RC = 1):
    backgroundPos = win.getMouse().getY()
    ground = Rectangle(Point(-4,0),Point(4,backgroundPos))
    ground.setFill("dark green")
    sky = Rectangle(Point(-4,8),Point(4,backgroundPos))
    sky.setFill("light blue")
    sky.setOutline("light blue")
    ground.setOutline("dark green")
    sky.draw(win)
    ground.draw(win)
    trees = 0
    while (trees < maxTrees):
        startPt = win.getMouse()
        if (startPt.getY() < backgroundPos):
            trees+=1
            treescale = 1 - (math.fabs(startPt.getY()/backgroundPos))
            print(treescale)
            drawTree(win,level,startPt,L*treescale,direction,Brangle,sf,RC)

def clear(win):
    ###clears drawn elements on the GraphWin
    #put this here because fix ur game riot
    win.delete("all")
        

            

def main():
    win = GraphWin(title = 'The Nursery', height = 800, width = 800, autoflush = True)
    gard = GraphWin(title = 'The Garden', height = 800, width = 800, autoflush = True)
    win.setCoords(-4,0,4,8)
    gard.setCoords(-4,0,4,8)
    L = .5
    Brangle = 20
    sf = 0.5
    level = 10
    RC = .5
    inp = 0
    direction = 90
    drawTree(win,level,Point(0,0),L,direction,Brangle,sf,RC)
    while(inp != 8):
        print(""" Enter the input that corresponds with what you want to do
                Note that at the moment all the settings are where I believe the optimal tree is drawn
                1: Change branch length
                2: Change brangle
                3: Change Randomness coefficient
                4: Change Tree Direction
                5: Change Scale Factor
                6: Change Level 
                7: Start a garden with current parameters for trees
                8: Spray Pesticide

                ---------------------------------------------------------------------------------------------------------------------""")
        inp = int(input('Enter input here '))
        if inp == 1:
            L = float(input('Enter length here (0 to 1 is good range) '))
            clear(win)
            drawTree(win,level,Point(0,0),L,direction,Brangle,sf,RC)
        if inp == 2:
            Brangle = float(input('Enter brangle here '))
            clear(win)
            drawTree(win,level,Point(0,0),L,direction,Brangle,sf,RC)
        if inp == 3:
            RC = float(input('Enter randomness coefficient here (between 0 and 1) '))
            clear(win)
            drawTree(win,level,Point(0,0),L,direction,Brangle,sf,RC)
        if inp == 4:
            direction = float(input('Enter direction here '))
            clear(win)
            drawTree(win,level,Point(0,0),L,direction,Brangle,sf,RC)
        if inp == 5:
            sf = float(input('Enter scale factor here (0 to 1) '))
            clear(win)
            drawTree(win,level,Point(0,0),L,direction,Brangle,sf,RC)
        if inp == 6:
            level = int(input('Enter level here '))
            clear(win)
            drawTree(win,level,Point(0,0),L,direction,Brangle,sf,RC)
        if inp == 7:
            maxTrees = int(input('Enter numbr of trees you want in the garden here '))
            makeGarden(gard,maxTrees,level,L,direction,Brangle,sf,RC)
        if inp == 8:
            clear(win)
            clear(gard)

main()
            
            
        
    
