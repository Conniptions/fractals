'''
By Nathan Cantor
KochCurve.py
'''
import time
from fractalUtils import *
from graphics import *
import math as math


### this code is for drawing koch curves, also generates a distance thing, to run this you need to install the wolfram alpha api

def clear(win):
    ###clears drawn elements on the GraphWin
    #put this here because fix ur game riot
    win.delete("all")

### this function draws a koch curve using recursive logic
def drawKC(win,clrs,level,startPt,L,direction,KCangle = 60,):
    if level == 0:
            drawLine(win,startPt,L,direction)
            win.update() 
    else:
            sf = L/(2 * (1+ math.cos(KCangle * math.pi / 180)))
            drawKC(win,clrs,level-1,startPt,sf,direction,KCangle)
            p2 = startPt.clone()
            drawKC(win,clrs,level-1,startPt,sf,direction+KCangle,KCangle)
            p0 = startPt.clone()
            drawKC(win,clrs,level-1,startPt,sf,direction-KCangle,KCangle)
            p1= startPt.clone()
            tri = Polygon(p0,p1,p2)
            tri.draw(win)
            tri.setFill(clrs[level-1])
            drawKC(win,clrs,level-1,startPt,sf,direction,KCangle)

#generates a list of colors 
def genColor(n):
    big = 16777215
    inc = int(big/n)
    acc = 0
    lst = []
    for i in range(n+1):
        lst.append(acc)
        acc += inc
    for i in range(len(lst)):
         lst[i] = str(hex(lst[i]))
    lst.remove('0x0')
    for i in range(len(lst)):
        lst[i] = '#' + lst[i][2:]
    for i in range(len(lst)):
        while len(lst[i]) < 7:
            lst[i] = '#0' + lst[i][1:]
    return(lst)

#calculates important values to make sure the a koch polygon is formed properly 
def drawStar(win,level,sides,KCangle = 60):
    if level > 6:
        level = 6
    clrs = genColor(level)
    if sides == 1:
        drawKC(win,clrs,level,Point(-0.5,0),1,0,KCangle)
    else:
        dtheta = (180*(sides-2))/(sides)
        L = (math.sin(math.pi/sides))
        intint = 180 - (dtheta)
        intintrad = intint*(math.pi/180)
        startX = -math.sin(intintrad/2)/2
        startY = math.copysign((math.cos(intintrad/2))/2,1)
        startPt = (Point(startX,startY))
        direction = 0
        for i in range(sides):
            drawKC(win,clrs,level,startPt,L,direction,KCangle)
            direction-= (180-dtheta)
                
#does what you would expect, allows one to draw a customizable koch star
def main():
    win = GraphWin(title = 'Koch Brothers, math edition', height = 800, width = 800, autoflush = True)
    win.setCoords(-1,-1,1,1)
    nol = 0
    print("""this program draws an n sided koch polygon for n > 0 to a desired level. The polygon will then be colored. You can also ask for an assesment of how long
    your koch curve is""")
    level = int(input("Enter the desired level: "))
    sides = int(input("Enter the number of sides you want your koch polygon to have (1 for a normal curve): "))
    inp = 0
    drawStar(win,level,sides)
    print("""Input list:

        1: get facts about the length of your curve
        2: choose new level and redraw curve
        3: choose new # of sides and redraw
        4: choose new interior angle for koch curve and redraw
        5: close the program) """)
    while (inp !=5):
        inp = int(input("Enter input here: "))
        if inp == 1:
        #     L = (math.sin(math.pi/sides))
        #     dInches = sides*(4**level) * L
        #     giveInterestingLengths(dInches)
        #Some defunct code, cant gen new key bc I dont have flash LOL
        if inp == 2:
            level = int(input("Enter the desired level: "))
            clear(win)
            drawStar(win,level,sides)
        if inp == 3:
            sides = int(input("Enter the desired # of sides: "))
            clear(win)
            drawStar(win,level,sides)
        if inp == 4:
            KCangle = float(input("Enter the desired angle: "))
            clear(win)
            drawStar(win,level,sides,KCangle)

main()

            
        
