from graphics import*
from math import *

def f(x):
    return 1/(sin(x)+cos(x))

def plotF(w,xmin,xmax):
    x=xmin
    stepSize=((xmax-xmin)/w.getWidth())
    while x<=xmax:
        if (sin(x)+cos(x))== 0:
            x += stepSize
        Point(x,f(x)).draw(w)
        x+=stepSize

def fillGraph (w,xmin,ymin,xmax,ymax):
    xstep=4*((xmax-xmin)/w.getWidth())
    ystep=4*((ymax-ymin)/w.getHeight())
    x=xmin
    y=ymin
    n = 0
    v = ['red','orange','yellow','green' ,'blue','violet','snow1','black','tan1','sienna']
    while x<xmax:
        y=ymin
        while y<ymax:
            p = Point(x,y)
            p.setOutline(v[n%len(v)])
            p.draw(w)
            y+=ystep
            n+=1
            if n%10 == 0:
                w.update()
        x+=xstep

def fillGraphSweep (w,xmin,ymin,xmax,ymax,size):
    xstep=size*((xmax-xmin)/w.getWidth())
    ystep=((ymax-ymin)/w.getHeight())
    x=xmin
    y=ymin
    c = 0
    #n = 0
    #v = ['red','orange','yellow','green' ,'blue','violet','black','tan1','sienna']
    for i in range(size):
        x = xmin + (xstep/size)*i
        while x<=xmax:
            y=ymin
            while y<=ymax:
                c=0
                p = Point(x,y)
                if x > 0 and y > 0:
                    p.setOutline('red')
                elif x < 0 and y > 0:
                    p.setOutline('blue')
                elif x < 0 and y < 0:
                    p.setOutline('green')
                elif x > 0 and y < 0:
                    p.setOutline('yellow')
                else:
                    p.setOutline('black')
                #p.setOutline(v[n%len(v)])
                p.draw(w)
                y+=ystep
                #n+=1
            x+=xstep
            w.update()
        
        
        
def main():
    win = GraphWin('window',800,400,autoflush = False)
    win.setBackground('white')

    xmin= -10.0
    xmax = 10.0
    ymin = -5.0
    ymax = 5.0
    
#setcoords changes what points in window correspond to, doesnt change window
    win.setCoords(xmin,ymin,xmax,ymax)
    xaxis = Line(Point(xmax,0),Point(xmin,0))
    yaxis = Line(Point(0,ymax),Point(0,ymin))
    yaxis.setOutline('black')
    xaxis.setOutline('black')
    yaxis.draw(win)
    xaxis.draw(win)
    #circ=Circle(Point(0,0),1)
    #circ.setOutline('red')
    #circ.draw(win)
    win.getMouse()
    fillGraphSweep(win,xmin,ymin,xmax,ymax,8)
    
    win.getMouse()
    win.close()


if __name__ == "__main__":
    main()
